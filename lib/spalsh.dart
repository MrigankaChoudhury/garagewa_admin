import 'package:flutter/material.dart';
import 'package:garagewa_admin/Feature/Auth/auth_controller.dart';
import 'package:garagewa_admin/Feature/Auth/login.dart';
import 'package:garagewa_admin/widgets/bottom_navbar.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Splash extends StatefulWidget {
  const Splash({super.key});

  @override
  State<Splash> createState() => _SplashState();
}

class _SplashState extends State<Splash> with TickerProviderStateMixin {
  final _authController = AuthController();
  Widget _defaultPage = const Login();

  late final AnimationController _controller = AnimationController(
    duration: const Duration(seconds: 3),
    vsync: this,
  )..repeat(reverse: true);
  late final Animation<double> _animation = CurvedAnimation(
    parent: _controller,
    curve: Curves.easeInBack,
  );

  @override
  void initState() {
    super.initState();
    _navigateToApp();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  _navigateToApp() async {
    WidgetsFlutterBinding.ensureInitialized();

    _authController.checkSession().then((value) async {
      final SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      sharedPreferences.setString("userName", value.data['USER_NAME']);
      sharedPreferences.setString("userId", value.data['USER_ID']);
      _defaultPage = const BottomNavBar();
    }).catchError((e) async {
      final SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      sharedPreferences.clear();
      _defaultPage = const Login();
      throw e;
    });
    await Future.delayed(const Duration(milliseconds: 3500), () {});
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => _defaultPage));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FadeTransition(
        opacity: _animation,
        child: Stack(
          children: [
            Positioned(bottom: 0, child: Image.asset('assets/splash.png')),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/logo.png',
                  height: MediaQuery.of(context).size.height * 0.18,
                  width: MediaQuery.of(context).size.width * 1,
                ),
                const Text(
                  "Expert repairs, for intelligent car owners",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.2,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
