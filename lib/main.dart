import 'package:flutter/material.dart';
import 'package:garagewa_admin/Provider/toggle_password.dart';
import 'package:garagewa_admin/Provider/user_info.dart';
import 'package:garagewa_admin/spalsh.dart';
import 'package:garagewa_admin/widgets/routes.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});
  final routes = GenetrateRoutes();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context) => UserStore()),
          ChangeNotifierProvider(create: (context) => TogglePassword()),
        ],
        child: MaterialApp(
          title: 'GarageWa Admin',
          theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
            useMaterial3: true,
          ),
          routes: routes.routes(),
          home: const Splash(),
        ));
  }
}
