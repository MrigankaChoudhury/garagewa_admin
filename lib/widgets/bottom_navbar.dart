import 'package:flutter/material.dart';
import 'package:garagewa_admin/Feature/Dashboard/dashboard.dart';
import 'package:garagewa_admin/Feature/Orders/oder.dart';
import 'package:garagewa_admin/Feature/Profile/account.dart';
import 'package:garagewa_admin/utils/constant.dart';

class BottomNavBar extends StatefulWidget {
  const BottomNavBar({Key? key}) : super(key: key);

  @override
  State<BottomNavBar> createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  int _currentIndex = 0;
  final _pageController = PageController();
  DateTime? currentBackPressTime;
  bool hidden = false;

  void _onItemTapped(int index) {
    setState(() {
      _currentIndex = index;
      _pageController.animateToPage(index,
          duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        DateTime now = DateTime.now();

        if (currentBackPressTime == null ||
            now.difference(currentBackPressTime!) >
                const Duration(seconds: 2)) {
          currentBackPressTime = now;
          Constant.showSnackbar(
              "Press back button again to exit", context, failedColor);
          return false;
        }
        return true;
      },
      child: Scaffold(
        backgroundColor: const Color(0xFFF5F5F5),
        body: SafeArea(
            child: PageView(
          physics: const NeverScrollableScrollPhysics(),
          controller: _pageController,
          children: const [
            DashBoard(),
            Center(child: Text("Reposts")),
            Orders(),
            Center(child: Text("Users")),
            Account()
          ],
        )),
        bottomNavigationBar: Theme(
          data: Theme.of(context).copyWith(
            canvasColor: Colors.white,
          ),
          child: BottomNavigationBar(
            iconSize: 25,
            selectedFontSize: 14,
            unselectedFontSize: 10,
            selectedItemColor: primaryColor,
            unselectedItemColor: primaryColor,
            showUnselectedLabels: false,
            elevation: 5,
            currentIndex: _currentIndex,
            onTap: (val) {
              _onItemTapped(val);
            },
            items: const [
              BottomNavigationBarItem(
                label: "Dashboard",
                icon: Icon(
                  Icons.dashboard_outlined,
                ),
              ),
              BottomNavigationBarItem(
                label: "Reports",
                icon: Icon(Icons.menu_book_outlined),
              ),
              BottomNavigationBarItem(
                  label: "Orders", icon: Icon(Icons.car_crash_outlined)),
              BottomNavigationBarItem(
                label: "Users",
                icon: Icon(
                  Icons.groups_outlined,
                ),
              ),
              BottomNavigationBarItem(
                label: "Account",
                icon: Icon(
                  Icons.account_circle_outlined,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ExitConfirmationDialog extends StatelessWidget {
  const ExitConfirmationDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      surfaceTintColor: Colors.white,
      title: const Text('Exit Confirmation'),
      content: const Text('Are you sure you want to exit the app?'),
      actions: <Widget>[
        TextButton(
          child: const Text('No'),
          onPressed: () {
            Navigator.of(context).pop(
                false); // Dismiss the dialog and indicate that exit is canceled
          },
        ),
        TextButton(
          child: const Text('Yes'),
          onPressed: () {
            Navigator.of(context).pop(
                true); // Dismiss the dialog and indicate that exit is confirmed
          },
        ),
      ],
    );
  }
}
