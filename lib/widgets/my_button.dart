import 'package:flutter/material.dart';
import 'package:garagewa_admin/utils/constant.dart';

class MyButton extends StatelessWidget {
  final void Function() onTap;
  final bool? loading;
  final Widget child;

  const MyButton({
    Key? key,
    required this.onTap,
    required this.child,
    this.loading,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.9,
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              backgroundColor: primaryColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5))),
          onPressed: onTap,
          child: child),
    );
  }
}
