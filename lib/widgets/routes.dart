import 'package:flutter/material.dart';
import 'package:garagewa_admin/Feature/Auth/login.dart';
import 'package:garagewa_admin/widgets/bottom_navbar.dart';

class GenetrateRoutes {
  // static Route<dynamic> generateRoutes(RouteSettings settings) {
  //   final args = settings.arguments;
  // }

  Map<String, WidgetBuilder> routes() {
    return {
      '/bottomNavBar': (context) => const BottomNavBar(),
      '/login': (context) => const Login(),
    };
  }
}
