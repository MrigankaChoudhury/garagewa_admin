import 'package:flutter/material.dart';

class MyTextBox extends StatelessWidget {
  final TextEditingController? controller;
  final String? hintText;
  final bool? obsecure;
  final Icon? prefixIcon;
  final IconButton? sufixIcon;
  final Text? label;
  final bool? readOnly;
  final TextInputType? textInputType;

  const MyTextBox(
      {Key? key,
      this.controller,
      this.hintText,
      this.obsecure,
      this.prefixIcon,
      this.sufixIcon,
      this.readOnly,
      this.label,
      this.textInputType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      keyboardType: textInputType ?? TextInputType.text,
      obscureText: obsecure ?? false,
      readOnly: readOnly ?? false,
      decoration: InputDecoration(
          prefixIcon: prefixIcon,
          label: label,
          suffixIcon: sufixIcon,
          contentPadding:
              const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
          filled: true,
          fillColor: Colors.grey[100],
          focusedBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white)),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey.shade200)),
          hintText: hintText,
          hintStyle: const TextStyle(color: Colors.grey)),
    );
  }
}
