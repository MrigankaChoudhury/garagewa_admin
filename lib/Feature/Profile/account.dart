import 'package:flutter/material.dart';
import 'package:garagewa_admin/Feature/Auth/login.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Account extends StatelessWidget {
  const Account({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          ListTile(
            onTap: () async {
              SharedPreferences sharedPreferences =
                  await SharedPreferences.getInstance();
              sharedPreferences.clear();
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const Login()),
                  ModalRoute.withName('/login'));
            },
            title: Text("Log Out"),
          )
        ],
      ),
    );
  }
}
