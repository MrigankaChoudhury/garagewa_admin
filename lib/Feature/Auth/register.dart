import 'dart:async';
import 'dart:io' as Io;
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:garagewa_admin/Feature/Auth/auth_controller.dart';
import 'package:garagewa_admin/feature/Auth/login.dart';
import 'package:garagewa_admin/utils/constant.dart';
import 'package:garagewa_admin/widgets/my_button.dart';
import 'package:garagewa_admin/widgets/my_text_box.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:file_picker/file_picker.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pinput/pinput.dart';

class Register extends StatefulWidget {
  const Register({super.key});

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  late TapGestureRecognizer _recognizer;
  final _pageController = PageController();
  final _authController = AuthController();
  final emailController = TextEditingController();
  final firstnameController = TextEditingController();
  final lastnameController = TextEditingController();
  final phoneConroller = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();
  final workshopController = TextEditingController();
  bool verifyEmail = false;
  int _currentIndex = 0;
  bool _selectCard = false;
  String _aadhaarfilePath = "";
  String _panfilePath = "";
  String _gstFilePath = "";
  String aadhaarFile = "";
  String panFile = "";
  String getFile = "";
  String aadhaarNumber = "";
  String panNumber = "";
  double height = 50;

  double lat = 26.117421605088857;
  double long = 91.70892085880041;

  Map buttonText = {
    0: "Next",
    1: "Next",
    2: "Next",
    3: "Create Account",
    4: "Submit",
    5: "Next",
    6: "Confirm",
    7: "Submit"
  };

  final pinController = TextEditingController();
  final focusNode = FocusNode();
  final formKey = GlobalKey<FormState>();

  static const focusedBorderColor = primaryColor;
  static const fillColor = Color.fromRGBO(243, 246, 249, 0);
  static const borderColor = primaryColor;
  List<Placemark> placemarks = [];

  final defaultPinTheme = PinTheme(
    width: 50,
    height: 50,
    textStyle: const TextStyle(
      fontSize: 22,
      color: Color.fromRGBO(30, 60, 87, 1),
    ),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(5),
      border: Border.all(color: borderColor),
    ),
  );

  late GoogleMapController mapController;

  LatLng? _currentPosition;

  Future<void> getLocation() async {
    PermissionStatus permissionStatus = await _getLocationPermission();
    if (permissionStatus == PermissionStatus.granted) {
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      setState(() {
        _currentPosition = LatLng(position.latitude, position.longitude);
      });
    }
  }

  Future<void> getTapPosition(double latitude, double longitude) async {
    placemarks.clear();
    placemarks = await placemarkFromCoordinates(latitude, longitude);
    print(placemarks);
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  Future<void> pickAadhaarFile() async {
    PermissionStatus permissionStatus = await _getStoragePermission();
    if (permissionStatus == PermissionStatus.granted) {
      FilePickerResult? result = await FilePicker.platform
          .pickFiles(type: FileType.custom, allowedExtensions: ["pdf"]);
      if (result != null) {
        PlatformFile file = result.files.first;
        setState(() {
          _aadhaarfilePath = file.name;
        });
        aadhaarFile =
            "data:application/${result.files.single.extension};base64,${await Io.File(result.files.single.path as String).readAsBytes()}";
      }
    }
  }

  Future<void> pickgstFile() async {
    PermissionStatus permissionStatus = await _getStoragePermission();
    if (permissionStatus == PermissionStatus.granted) {
      FilePickerResult? result = await FilePicker.platform
          .pickFiles(type: FileType.custom, allowedExtensions: ["jpg"]);
      if (result != null) {
        PlatformFile file = result.files.first;
        setState(() {
          _gstFilePath = file.name;
        });
        getFile =
            "data:application/${result.files.single.extension};base64,${await Io.File(result.files.single.path as String).readAsBytes()}";
      }
    }
  }

  Future<void> pickPanFile() async {
    PermissionStatus permissionStatus = await _getStoragePermission();
    if (permissionStatus == PermissionStatus.granted) {
      FilePickerResult? result = await FilePicker.platform
          .pickFiles(type: FileType.custom, allowedExtensions: ["jpg"]);
      if (result != null) {
        setState(() {
          _panfilePath = result.files.single.name;
        });
        panFile =
            "data:application/${result.files.single.extension};base64,${await Io.File(result.files.single.path as String).readAsBytes()}";
      }
    }
  }

  Future<PermissionStatus> _getStoragePermission() async {
    PermissionStatus permission = await Permission.storage.status;
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.permanentlyDenied) {
      PermissionStatus permissionStatus = await Permission.storage.request();
      return permissionStatus;
    } else {
      return permission;
    }
  }

  Future<PermissionStatus> _getLocationPermission() async {
    PermissionStatus permission = await Permission.location.status;
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.permanentlyDenied) {
      PermissionStatus permissionStatus = await Permission.location.request();
      return permissionStatus;
    } else {
      return permission;
    }
  }

  @override
  void initState() {
    super.initState();
    getLocation();
    _recognizer = TapGestureRecognizer()..onTap = _handleTap;
  }

  @override
  void dispose() {
    super.dispose();
    _recognizer.dispose();
    _pageController.dispose();
  }

  void _handleTap() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => const Login()));
  }

  void _setSelectCard() {
    setState(() {
      _selectCard = !_selectCard;
    });
  }

  void _onItemTapped(int index) {
    if (index == 2) {
      if (firstnameController.text.isEmpty ||
          lastnameController.text.isEmpty ||
          phoneConroller.text.isEmpty) {
        Constant.showSnackbar("Flii all the fields", context, failedColor);
      } else if (!verifyEmail) {
        Constant.showSnackbar(
            "Please verify email first", context, failedColor);
      } else {
        setState(() {
          _currentIndex = index;
          _pageController.animateToPage(index,
              duration: const Duration(milliseconds: 500),
              curve: Curves.easeOut);
        });
      }
    } else if (index == 4) {
      print("hello");
      print(passwordController.text);
      print(confirmPasswordController.text);
      print(workshopController.text);
      if (passwordController.text.isEmpty ||
          confirmPasswordController.text.isEmpty ||
          workshopController.text.isEmpty) {
        Constant.showSnackbar(
            "Please enter password and confirm password", context, failedColor);
      } else if (passwordController.text != confirmPasswordController.text) {
        Constant.showSnackbar(
            "Please enter same password ", context, failedColor);
      } else {
        Map data = {
          "firstName": firstnameController.text,
          "lastName": lastnameController.text,
          "password": passwordController.text,
          "phone": {"phone": "+91${phoneConroller.text}"},
          "email": {"email": emailController.text},
          "workshopName": workshopController.text,
          "address": {
            "addressLine":
                "${placemarks[0].thoroughfare} ${placemarks[0].subLocality} ${placemarks[0].locality}",
            "locality": placemarks[0].subLocality,
            "state": placemarks[0].administrativeArea,
            "city": placemarks[0].locality,
            "pin": placemarks[0].postalCode,
            "lat": lat,
            "lng": long
          }
        };
        _authController.register(data).then((value) {
          setState(
            () {
              _currentIndex = index;
              _pageController.animateToPage(index,
                  duration: const Duration(milliseconds: 500),
                  curve: Curves.easeOut);
            },
          );
        });
      }
    } else {
      setState(() {
        _currentIndex = index;
        _pageController.animateToPage(index,
            duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              if (_currentIndex > 0) {
                _onItemTapped(_currentIndex - 1);
              } else {
                Navigator.pop(context);
              }
            },
            icon: const Icon(Icons.arrow_back)),
        centerTitle: true,
        title: SizedBox(height: height, child: Image.asset('assets/logo.png')),
        toolbarHeight: height,
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 15, right: 15, top: 30),
        child: PageView(
          scrollDirection: Axis.horizontal,
          controller: _pageController,
          physics: const NeverScrollableScrollPhysics(),
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  'Welcome to GarageWa Operations Management System',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                ),
                const SizedBox(
                  height: 30,
                ),
                const Text(
                  "1. Choose an account type",
                  style: TextStyle(fontWeight: FontWeight.w600),
                ),
                const SizedBox(
                  height: 20,
                ),
                GestureDetector(
                  onTap: _setSelectCard,
                  child: Card(
                    surfaceTintColor: Colors.white,
                    shape: RoundedRectangleBorder(
                        side: BorderSide(
                            color: _selectCard ? primaryColor : Colors.white),
                        borderRadius: BorderRadius.circular(
                          10,
                        )),
                    child: const Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Vendor",
                            style: TextStyle(fontWeight: FontWeight.w600),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            "For vendors who want to run their own car service workshops and manage a fleet of mechanics and service providers",
                            style: TextStyle(fontSize: 12),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                GestureDetector(
                  onTap: _setSelectCard,
                  child: Card(
                    surfaceTintColor: Colors.white,
                    shape: RoundedRectangleBorder(
                        side: BorderSide(
                            color: !_selectCard ? primaryColor : Colors.white),
                        borderRadius: BorderRadius.circular(
                          10,
                        )),
                    child: const Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Individual Mechanic or Service Provider",
                            style: TextStyle(fontWeight: FontWeight.w600),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            "For individual vendors and service providers who works independently and provide their services as needed",
                            style: TextStyle(fontSize: 12),
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "1. Add your personal information",
                    style: TextStyle(fontWeight: FontWeight.w600),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  MyTextBox(
                    controller: firstnameController,
                    hintText: "First Name",
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  MyTextBox(
                    controller: lastnameController,
                    hintText: "Last Name",
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  MyTextBox(
                    controller: phoneConroller,
                    hintText: "Phone Number",
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  MyTextBox(
                    hintText: "Email",
                    controller: emailController,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.blueAccent,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                            ),
                          ),
                          onPressed: verifyEmail
                              ? null
                              : () {
                                  if (emailController.text.isEmpty) {
                                    Constant.showSnackbar(
                                        "Please enter a email",
                                        context,
                                        failedColor);
                                  } else {
                                    _authController
                                        .verifyEmail(emailController.text)
                                        .then((value) {
                                      showModel(context);
                                    });
                                  }
                                },
                          child: Text(
                            verifyEmail ? "Verified" : "Verify Email",
                            style: const TextStyle(color: Colors.white),
                          ))
                    ],
                  )
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  "2. Select Address",
                  style: TextStyle(fontWeight: FontWeight.w600),
                ),
                const SizedBox(
                  height: 20,
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.7,
                  child: GoogleMap(
                    onMapCreated: _onMapCreated,
                    initialCameraPosition: CameraPosition(
                      target: _currentPosition ??
                          const LatLng(26.117421605088857, 91.70892085880041),
                      zoom: 16.0,
                    ),
                    onTap: ((agr) {
                      setState(() {
                        lat = agr.latitude;
                        long = agr.longitude;
                      });
                      getTapPosition(agr.latitude, agr.longitude);
                    }),
                    myLocationEnabled: true,
                    markers: {
                      Marker(
                          markerId: const MarkerId("Tab Location"),
                          icon: BitmapDescriptor.defaultMarker,
                          position: LatLng(lat, long),
                          draggable: true,
                          onDragEnd: ((val) {
                            lat = val.latitude;
                            long = val.longitude;
                          }))
                    },
                  ),
                )
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  "3. Set your password",
                  style: TextStyle(fontWeight: FontWeight.w600),
                ),
                const SizedBox(
                  height: 20,
                ),
                MyTextBox(
                  controller: passwordController,
                  hintText: "Password",
                  obsecure: true,
                  sufixIcon: IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.visibility_off,
                      size: 15,
                      color: primaryColor,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                MyTextBox(
                  controller: confirmPasswordController,
                  hintText: "Confirm Password",
                  obsecure: true,
                  sufixIcon: IconButton(
                      onPressed: () {},
                      icon: const Icon(
                        Icons.visibility_off,
                        size: 15,
                        color: primaryColor,
                      )),
                ),
                const SizedBox(
                  height: 20,
                ),
                const Text(
                  "3. Add your workshop",
                  style: TextStyle(fontWeight: FontWeight.w600),
                ),
                MyTextBox(
                  controller: workshopController,
                  hintText: "Workshop Name",
                ),
              ],
            ),
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Your account has been created successfully.",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  const Text(
                    "Now quickly complete the KYC process to get access to GarageWa OMS",
                    style: TextStyle(color: primaryColor),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  const Text(
                    "Upload Documnets",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        height: 100,
                        width: 100,
                        child: GestureDetector(
                          onTap: () {
                            pickAadhaarFile();
                          },
                          child: DottedBorder(
                            radius: const Radius.circular(10),
                            borderType: BorderType.RRect,
                            color: Colors.black,
                            strokeWidth: 1,
                            dashPattern: const [8, 8],
                            child: Image.asset('assets/doc.png'),
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Flexible(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const MyTextBox(
                              hintText: "AADHAR",
                            ),
                            Text(_aadhaarfilePath)
                          ],
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        height: 100,
                        width: 100,
                        child: GestureDetector(
                          onTap: () {
                            pickPanFile();
                          },
                          child: DottedBorder(
                            borderType: BorderType.RRect,
                            radius: const Radius.circular(12),
                            color: Colors.black,
                            dashPattern: const [8, 8],
                            strokeWidth: 1,
                            child: Image.asset('assets/doc.png'),
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Flexible(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const MyTextBox(
                              hintText: "PAN",
                            ),
                            Text(_panfilePath)
                          ],
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        height: 100,
                        width: 100,
                        child: GestureDetector(
                          onTap: () {
                            pickgstFile();
                          },
                          child: DottedBorder(
                            borderType: BorderType.RRect,
                            radius: const Radius.circular(12),
                            color: Colors.black,
                            dashPattern: const [8, 8],
                            strokeWidth: 1,
                            child: Image.asset('assets/doc.png'),
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Flexible(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const MyTextBox(
                              hintText: "GST",
                            ),
                            Text(_gstFilePath)
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            const Column(
              children: [
                Text(
                  "KYC documents are submitted successfully.",
                  style: TextStyle(fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  "While we review your KYC documents, please add your service expertise",
                  style: TextStyle(color: primaryColor),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  "Select Expertise as per Services",
                  style: TextStyle(fontWeight: FontWeight.w600),
                )
              ],
            )
          ],
        ),
      ),
      bottomSheet: SizedBox(
        height: MediaQuery.of(context).size.height * 0.07,
        child: Padding(
          padding: const EdgeInsets.only(bottom: 10, left: 10, right: 10),
          child: MyButton(
            onTap: () {
              _onItemTapped(_currentIndex + 1);
            },
            child: Text(buttonText[_currentIndex]),
          ),
        ),
      ),
    );
  }

  Future<dynamic> showModel(
    BuildContext context,
  ) {
    return showModalBottomSheet(
        isScrollControlled: true,
        showDragHandle: true,
        context: context,
        builder: (context) {
          return Padding(
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: SizedBox(
              height: MediaQuery.of(context).size.height * 0.3,
              child: Stack(
                alignment: AlignmentDirectional.center,
                children: [
                  Positioned(
                    top: 0,
                    child: Column(
                      children: [
                        const Text(
                          "Verify OTP",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.w500),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Directionality(
                              // Specify direction if desired
                              textDirection: TextDirection.ltr,
                              child: Pinput(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                length: 6,
                                controller: pinController,
                                focusNode: focusNode,
                                androidSmsAutofillMethod:
                                    AndroidSmsAutofillMethod.none,
                                listenForMultipleSmsOnAndroid: true,
                                defaultPinTheme: defaultPinTheme,
                                separatorBuilder: (index) => SizedBox(
                                    width: MediaQuery.of(context).size.width *
                                        0.005),
                                hapticFeedbackType:
                                    HapticFeedbackType.lightImpact,
                                onCompleted: (pin) {
                                  debugPrint('onCompleted: $pin');
                                },
                                onChanged: (value) {
                                  debugPrint('onChanged: $value');
                                },
                                cursor: Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Container(
                                      margin: const EdgeInsets.only(bottom: 10),
                                      width: 10,
                                      height: 1,
                                      color: focusedBorderColor,
                                    ),
                                  ],
                                ),
                                focusedPinTheme: defaultPinTheme.copyWith(
                                  decoration:
                                      defaultPinTheme.decoration!.copyWith(
                                    borderRadius: BorderRadius.circular(5),
                                    border:
                                        Border.all(color: focusedBorderColor),
                                  ),
                                ),
                                submittedPinTheme: defaultPinTheme.copyWith(
                                  decoration:
                                      defaultPinTheme.decoration!.copyWith(
                                    color: fillColor,
                                    borderRadius: BorderRadius.circular(5),
                                    border:
                                        Border.all(color: focusedBorderColor),
                                  ),
                                ),
                                errorPinTheme: defaultPinTheme.copyBorderWith(
                                  border: Border.all(color: Colors.redAccent),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.07,
                      width: MediaQuery.of(context).size.width * 1,
                      color: Colors.white,
                      child: Padding(
                        padding:
                            const EdgeInsets.only(left: 5, right: 5, bottom: 5),
                        child: MyButton(
                            onTap: () {
                              Navigator.pop(context);
                              _authController
                                  .verifyOtp(
                                      emailController.text, pinController.text)
                                  .then((value) {
                                setState(() {
                                  verifyEmail = true;
                                });
                              }).catchError((e) {});
                            },
                            child:const Text("Verify OTP")),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
