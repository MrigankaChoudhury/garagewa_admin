import 'package:dio/dio.dart';
import 'package:garagewa_admin/services/dio.dart';

class AuthController {
  final _dioClient = HTTP();

  Future<Response> signIn(String username, String password) async {
    try {
      final response = await _dioClient.post(
          url: "/ga-auth/signIn",
          data: {"username": username, "password": password});
      print(response);
      return response;
    } catch (e) {
      rethrow;
    }
  }

  Future<Response> checkSession() async {
    try {
      final response = await _dioClient.get(
        url: "/ga-auth/authentication/checkSession",
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  Future<Response> verifyEmail(String email) async {
    try {
      final response = await _dioClient
          .post(url: "/ga-vendor/vendors/getOtp", data: {"key": email});
      return response;
    } catch (e) {
      rethrow;
    }
  }

  Future<Response> verifyOtp(String email, String otp) async {
    try {
      final response = await _dioClient.post(
          url: "/ga-vendor/vendors/verifyOtp",
          data: {"key": email, "value": otp});
      return response;
    } catch (e) {
      rethrow;
    }
  }

  Future<Response> register(Map data) async {
    print(data);
    try {
      final response =
          await _dioClient.post(url: "/ga-vendor/vendors/signUp", data: data);
      return response;
    } catch (e) {
      rethrow;
    }
  }
}
