import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:garagewa_admin/Feature/Auth/auth_controller.dart';
import 'package:garagewa_admin/Provider/toggle_password.dart';
import 'package:garagewa_admin/Provider/user_info.dart';
import 'package:garagewa_admin/feature/Auth/register.dart';
import 'package:garagewa_admin/utils/constant.dart';
import 'package:garagewa_admin/widgets/bottom_navbar.dart';
import 'package:garagewa_admin/widgets/my_button.dart';
import 'package:garagewa_admin/widgets/my_text_box.dart';
import 'package:provider/provider.dart';

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  late TapGestureRecognizer _recognizer;
  final _auhController = AuthController();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  bool loading = false;

  Future<void> validateAuth() async {
    print("object");
    setState(() {
      loading = true;
    });
    final userProvider = Provider.of<UserStore>(context, listen: false);
    _auhController
        .signIn(_usernameController.text, _passwordController.text)
        .then((value) {
      setState(() {
        loading = false;
      });
      final setCookie = value.headers["set-cookie"]![0];
      print(setCookie);
      userProvider.addUserInfo(value.data['USER_ID'], value.data['USER_NAME'],
          value.data['USER_ROLE'], setCookie);
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => const BottomNavBar(),
        ),
      );
    }).catchError((e) {
      setState(() {
        loading = false;
      });
      Constant.showSnackbar(e.response!.data['message'], context, failedColor);
    });
  }

  @override
  void initState() {
    super.initState();
    _recognizer = TapGestureRecognizer()..onTap = _handleTap;
  }

  @override
  void dispose() {
    super.dispose();
    _recognizer.dispose();
  }

  void _handleTap() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => const Register()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 40),
          child: Consumer<TogglePassword>(builder: (context, store, child) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset('assets/logo.png'),
                const SizedBox(
                  height: 30,
                ),
                const Text(
                  "Sign in to GarageWa Vendor",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(
                  height: 15,
                ),
                const Text(
                  "Manage orders, mechanics and",
                  textAlign: TextAlign.center,
                ),
                const Text(
                  "providers all in one place",
                  textAlign: TextAlign.center,
                ),
                const SizedBox(
                  height: 60,
                ),
                MyTextBox(
                  controller: _usernameController,
                  hintText: "Username",
                ),
                const SizedBox(
                  height: 30,
                ),
                MyTextBox(
                  controller: _passwordController,
                  hintText: "Password",
                  obsecure: store.loginPassword,
                  sufixIcon: IconButton(
                      onPressed: () {
                        store.showHideLoginPassword();
                      },
                      icon: store.loginPassword
                          ? const Icon(
                              Icons.visibility_off,
                              size: 15,
                              color: primaryColor,
                            )
                          : const Icon(
                              Icons.visibility,
                              size: 15,
                              color: primaryColor,
                            )),
                ),
                const SizedBox(
                  height: 30,
                ),
                MyButton(
                    onTap: () {
                      if (_usernameController.text.isEmpty ||
                          _passwordController.text.isEmpty) {
                        Constant.showSnackbar(
                            "Please fill all the fields", context, failedColor);
                      } else {
                        validateAuth();
                      }
                    },
                    child: loading
                        ? const CircularProgressIndicator()
                        : const Text(
                            "Login",
                            style: TextStyle(color: Colors.white),
                          )),
                const SizedBox(
                  height: 40,
                ),
                RichText(
                    textAlign: TextAlign.start,
                    text: TextSpan(
                        text: "Don’t have an account?",
                        style: const TextStyle(color: Colors.black),
                        children: [
                          TextSpan(
                              text: " Sign up now",
                              style: const TextStyle(color: primaryColor),
                              recognizer: _recognizer)
                        ])),
                const SizedBox(
                  height: 40,
                ),
                RichText(
                    textAlign: TextAlign.start,
                    text: TextSpan(
                        text:
                            "By continuing, you are indicating that you have read and agreed to the",
                        style: const TextStyle(color: Colors.black),
                        children: [
                          TextSpan(
                              text: " Terms and Conditions and Privacy Policy.",
                              style: const TextStyle(color: primaryColor),
                              recognizer: _recognizer)
                        ]))
              ],
            );
          }),
        ),
      ),
    );
  }
}
