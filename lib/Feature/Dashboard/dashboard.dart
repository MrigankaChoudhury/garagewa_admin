import 'package:flutter/material.dart';
import 'package:garagewa_admin/utils/constant.dart';

class DashBoard extends StatefulWidget {
  const DashBoard({super.key});

  @override
  State<DashBoard> createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {
  String startdate = "November 4,2023";
  String lastdate = "December 4,2023";
  bool enableProfile = true;
  DateTime start = DateTime(DateTime.now().year, DateTime.now().month, 1);
  DateTime end =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);

  void setlastDate() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: Image.asset(
            'assets/logo.png',
            width: 40,
            height: 20,
          ),
        ),
        actions: [
          const Text("Action"),
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Switch(
                activeColor: primaryColor,
                value: enableProfile,
                onChanged: (val) {
                  setState(() {
                    enableProfile = val;
                  });
                }),
          )
        ],
      ),
      body: const Column(
        children: [],
      ),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: primaryColor,
        onPressed: () {
          showModalBottomSheet(
              showDragHandle: true,
              context: context,
              builder: (context) {
                return SizedBox(
                  child: Column(children: [
                    const Text(
                      "Filter",
                      style:
                          TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {},
                            child: Card(
                              color: primaryColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5)),
                              child: const SizedBox(
                                  height: 80,
                                  width: 100,
                                  child: Center(
                                      child: Text(
                                    "1D",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600),
                                  ))),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {},
                            child: Card(
                              color: primaryColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5)),
                              child: const SizedBox(
                                height: 80,
                                width: 100,
                                child: Center(
                                  child: Text(
                                    "1W",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {},
                            child: Card(
                              color: primaryColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5)),
                              child: const SizedBox(
                                height: 80,
                                width: 100,
                                child: Center(
                                  child: Text(
                                    "1M",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () async {
                              DateTime? startDate = await showDatePicker(
                                  context: context,
                                  firstDate: DateTime(1900),
                                  lastDate: DateTime(2100),
                                  initialDate: start);
                              setState(() {
                                if (startDate == null) return;
                                start = startDate;
                              });
                            },
                            child: Expanded(
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5)),
                                child: Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(20.0),
                                    child: Row(
                                      children: [
                                        const Icon(
                                          Icons.calendar_month_outlined,
                                          color: primaryColor,
                                        ),
                                        Text(
                                          startdate,
                                          style: const TextStyle(
                                              color: Colors.black),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () async {
                              DateTime? endDate = await showDatePicker(
                                  context: context,
                                  firstDate: DateTime(1900),
                                  lastDate: DateTime(2100),
                                  initialDate: end);
                              setState(() {
                                if (endDate == null) return;
                                end = endDate;
                              });
                            },
                            child: Expanded(
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5)),
                                child: Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(20.0),
                                    child: Row(
                                      children: [
                                        const Icon(
                                          Icons.calendar_month_outlined,
                                          color: primaryColor,
                                        ),
                                        Text(
                                          lastdate,
                                          style: const TextStyle(
                                              color: Colors.black),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ]),
                );
              });
        },
        icon: const Icon(
          Icons.filter_alt_outlined,
          color: Colors.white,
        ),
        label: const Text(
          "Filter",
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }
}
