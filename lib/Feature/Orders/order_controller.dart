import 'package:dio/dio.dart';
import 'package:garagewa_admin/services/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OrderController {
  final _dioClient = HTTP();
  Future<Response> getOrder() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    final userId = sharedPreferences.getString("userId");
    try {
      final response = _dioClient.get(
          url: "/ga-order/customerOrders/findByWorkshopId?id=$userId");
      return response;
    } catch (e) {
      rethrow;
    }
  }

  Future<Response> getOrderDetails(String orderId) async {
    try {
      final response = _dioClient.get(
          url: "/ga-order/customerOrders/findByOrderId?orderId=$orderId");
      return response;
    } catch (e) {
      rethrow;
    }
  }

  // Future<Response> getOrderDetails(String orderId) async {
  //   try {
  //     final response = _dioClient.get(
  //         url: "/ga-order/customerOrders/findByOrderId?orderId=$orderId");
  //     return response;
  //   } catch (e) {
  //     rethrow;
  //   }
  // }
}
