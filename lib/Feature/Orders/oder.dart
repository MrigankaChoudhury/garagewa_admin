import 'package:flutter/material.dart';
import 'package:garagewa_admin/Feature/Orders/order_controller.dart';
import 'package:garagewa_admin/Feature/Orders/order_details.dart';
import 'package:garagewa_admin/utils/constant.dart';

class Orders extends StatefulWidget {
  const Orders({super.key});

  @override
  State<Orders> createState() => _OrdersState();
}

class _OrdersState extends State<Orders> {
  List order = [];
  bool loading = true;
  final _orderController = OrderController();
  Future<void> getOrder() async {
    _orderController.getOrder().then((value) {
      order = value.data['content'];
      setState(() {
        loading = false;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getOrder();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        scrolledUnderElevation: 0,
        title: const Text("Order Management"),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
        child: ListView(
          children: [
            const Text(
              "Active Order",
              style: TextStyle(fontSize: 18, color: primaryColor),
            ),
            ...order.map((e) {
              return Padding(
                padding: const EdgeInsets.only(top: 10),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => OrderDetails(
                          orderId: e,
                        ),
                      ),
                    );
                  },
                  child: Container(
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(0),
                        border: Border.all(width: 0.5)),
                    child: Text(
                      "$e",
                      style: const TextStyle(fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              );
            })
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Colors.white,
        onPressed: () {
          showModalBottomSheet(
              context: context,
              builder: (context) {
                return SizedBox(
                  height: MediaQuery.of(context).size.height * 0.5,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(
                        height: 30,
                      ),
                      Row(
                        children: [
                          Checkbox(value: false, onChanged: (val) {}),
                          const Text("Active Orders")
                        ],
                      ),
                      Row(
                        children: [
                          Checkbox(value: false, onChanged: (val) {}),
                          const Text("Rescheduled Orders")
                        ],
                      ),
                      Row(
                        children: [
                          Checkbox(value: false, onChanged: (val) {}),
                          const Text("SOS Orders")
                        ],
                      ),
                      Row(
                        children: [
                          Checkbox(value: false, onChanged: (val) {}),
                          const Text("Disputed Orders")
                        ],
                      ),
                      Row(
                        children: [
                          Checkbox(value: false, onChanged: (val) {}),
                          const Text("Completed Orders")
                        ],
                      ),
                    ],
                  ),
                );
              });
        },
        icon: const Icon(
          Icons.filter_list,
          color: primaryColor,
        ),
        label: const Text("Filter"),
      ),
    );
  }
}
