import 'package:flutter/material.dart';
import 'package:garagewa_admin/Feature/Orders/order_controller.dart';

class OrderDetails extends StatefulWidget {
  final String orderId;
  const OrderDetails({super.key, required this.orderId});

  @override
  State<OrderDetails> createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {
  Map orderDetails = {};
  bool loading = true;
  final _orderController = OrderController();
  Future<void> getOrderDetails() async {
    _orderController.getOrderDetails(widget.orderId).then((value) {
      orderDetails = value.data;
      setState(() {
        loading = false;
      });
    }).catchError((e) {
      setState(() {
        loading = false;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getOrderDetails();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
          appBar: AppBar(
            title: Text(widget.orderId),
            bottom: const TabBar(tabs: [
              Tab(
                text: "Order Details",
              ),
              Tab(
                text: "Job Card",
              ),
              Tab(
                text: "Invoice",
              )
            ]),
          ),
          body: loading == true
              ? const Center(child:  CircularProgressIndicator())
              : SingleChildScrollView(
                  child: Column(children: [
                    Card(
                      surfaceTintColor: Colors.white,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            color: const Color(0xFF221A4A),
                            height: 50,
                            child: const Center(
                              child: Text(
                                "Order Details",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text("Customer Name"),
                                Text(orderDetails['customer']['customerName'] ??
                                    "")
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text("Order ID"),
                                Text(orderDetails['orderId'] ?? "")
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text("Order Status"),
                                Text(orderDetails['orderStatus'] ?? "")
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text("Order Date"),
                                Text(orderDetails['dateTime'] ?? "")
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text("Service Date"),
                                Text(orderDetails['dateOfService'] ?? "")
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Card(
                      surfaceTintColor: Colors.white,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            color: const Color(0xFF221A4A),
                            height: 50,
                            child: const Center(
                              child: Text(
                                "Car Details",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text("Brand"),
                                Text(orderDetails['car']['brandName'] ?? "")
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text("Model"),
                                Text(orderDetails['car']['modelName'] ?? "")
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text("Fuel Type"),
                                Text(orderDetails['car']['fuelType'] ?? "")
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text("Engine Type"),
                                Text(orderDetails['car']['engineType'] ?? "")
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text("Registration Number"),
                                Text(orderDetails['car']
                                        ['registrationNumber'] ??
                                    "")
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Card(
                      surfaceTintColor: Colors.white,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            color: const Color(0xFF221A4A),
                            height: 50,
                            child: const Center(
                              child: Text(
                                "Address",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Expanded(child: Text("Address")),
                                Expanded(
                                  flex: 1,
                                  child: Text(
                                    orderDetails['address']['addressLine'] ??
                                        "",
                                    maxLines: 5,
                                    textAlign: TextAlign.end,
                                  ),
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text("City"),
                                Text(orderDetails['address']['city'] ?? "")
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text("Locality"),
                                Text(orderDetails['address']['locality'] ?? "")
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text("Pin"),
                                Text(orderDetails['address']['pin'] ?? "")
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text("State"),
                                Text(orderDetails['address']['state'] ?? "")
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Column(
                      children: [
                        Container(
                          color: const Color(0xFF221A4A),
                          height: 50,
                          child: const Center(
                            child: Text(
                              "Invoice Details",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Text(
                                    "Service Item",
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                  ),
                                  ...orderDetails['requestService'].map((e) {
                                    return Text(e['serviceName'] ?? "");
                                  }),
                                  const Text(
                                    "Total",
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                  )
                                ],
                              ),
                              Column(
                                children: [
                                  const Text(
                                    "Discount",
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                  ),
                                  ...orderDetails['requestService'].map((e) {
                                    return const Text("XXXXXXX");
                                  }),
                                  const Text(
                                    "XXXXXXX",
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                  )
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  const Text(
                                    "Net Price",
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                  ),
                                  ...orderDetails['requestService'].map((e) {
                                    return Text("${e['estimatePrice']}");
                                  }),
                                  Text(
                                    "${orderDetails['subTotal']}",
                                    style: const TextStyle(
                                        fontWeight: FontWeight.w600),
                                  )
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 40,
                    )
                  ]),
                )),
    );
  }
}
