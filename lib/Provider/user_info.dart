import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserStore extends ChangeNotifier {
  String _userId = "";
  String get userId => _userId;
  String _username = "";
  String get username => _username;
  String _role = "";
  String get role => _role;

  void addUserInfo(
      String userId, String username, String role, String cookie) async {
    _userId = userId;
    _username = username;
    _role = role;
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("username", _username);
    sharedPreferences.setString("userId", _userId);
    sharedPreferences.setString("role", _role);
    sharedPreferences.setString("cookie", cookie);
    notifyListeners();
  }
}
