import 'package:flutter/material.dart';

class TogglePassword extends ChangeNotifier {
  bool _loginPassword = true;
  bool get loginPassword => _loginPassword;

  void showHideLoginPassword() {
    _loginPassword = !loginPassword;
    notifyListeners();
  }
}
