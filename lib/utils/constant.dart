import 'dart:convert';

import 'package:flutter/material.dart';

const primaryColor = Color(0xFFE05354);
const successColor = Color(0xFF43A047);
const failedColor = Color(0xFF0F1923);
const backgroundColor = Color(0xFFF5F5F5);

const String clientId = "garageWa";
const String clientSecret = "garageWa321\$";
const String baseUrl = "https://stageapi.garagewa.com";
const String origin = "https://gaportal.garagewa.com:3000";

final clientString = base64Encode(utf8.encode("$clientId:$clientSecret"));
String clientIdSecret = "Basic $clientString";

Map day = {
  1: "Monday",
  2: "Tuesday",
  3: "Wednessday",
  4: "Thursday",
  5: "Friday",
  6: "Saturday",
  7: "Sunday"
};

Map time = {
  1: "1",
  2: "2",
  3: "3",
  4: "4",
  5: "5",
  6: "6",
  7: "7",
  8: "8",
  9: "9",
  10: "10",
  11: "11",
  12: "12",
  13: "1",
  14: "2",
  15: "3",
  16: "4",
  17: "5",
  18: "6",
  19: "7",
  20: "8",
  21: "9",
  22: "10",
  23: "11",
  24: "12"
};

Map month = {
  1: "Jan",
  2: "Fab",
  3: "Mar",
  4: "April",
  5: "May",
  6: "Jun",
  7: "Jul",
  8: "Aug",
  9: "Sep",
  10: "Oct",
  11: "Nov",
  12: "Dec"
};

class Constant {
  static void showSnackbar(String e, BuildContext context, Color color) {
    final snackBar = SnackBar(
      behavior: SnackBarBehavior.floating,
      duration: const Duration(milliseconds: 800),
      backgroundColor: color,
      content: Text(
        e,
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
